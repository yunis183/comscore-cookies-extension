// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
if (!chrome.cookies) {
  chrome.cookies = chrome.experimental.cookies;
}
var whiteCookies;
var allCookies;
var allDomains = [];
// A simple Timer class.
function Timer() {
  this.start_ = new Date();
  this.elapsed = function() {
    return (new Date()) - this.start_;
  }
  this.reset = function() {
    this.start_ = new Date();
  }
}
// Compares cookies for "key" (name, domain, etc.) equality, but not "value"
// equality.
function cookieMatch(c1, c2) {
  return (c1.name == c2.name) && (c1.domain == c2.domain) && (c1.hostOnly == c2.hostOnly) && (c1.path == c2.path) && (c1.secure == c2.secure) && (c1.httpOnly == c2.httpOnly) && (c1.session == c2.session) && (c1.storeId == c2.storeId);
}
// Returns an array of sorted keys from an associative array.
function sortedKeys(array) {
  var keys = [];
  for (var i in array) {
    keys.push(i);
  }
  keys.sort();
  return keys;
}
// Shorthand for document.querySelector.
function select(selector) {
  return document.querySelector(selector);
}
// An object used for caching data about the browser's cookies, which we update
// as notifications come in.
function CookieCache() {
  this.cookies_ = {};
  this.reset = function() {
    this.cookies_ = {};
  }
  this.add = function(cookie) {
    var domain = cookie.domain;
    if (!this.cookies_[domain]) {
      this.cookies_[domain] = [];
    }
    this.cookies_[domain].push(cookie);
  };
  this.remove = function(cookie) {
    var domain = cookie.domain;
    if (this.cookies_[domain]) {
      var i = 0;
      while (i < this.cookies_[domain].length) {
        if (cookieMatch(this.cookies_[domain][i], cookie)) {
          this.cookies_[domain].splice(i, 1);
        } else {
          i++;
        }
      }
      if (this.cookies_[domain].length == 0) {
        delete this.cookies_[domain];
      }
    }
  };
  // Returns a sorted list of cookie domains that match |filter|. If |filter| is
  //  null, returns all domains.
  this.getDomains = function(filter) {
    var result = [];
    sortedKeys(this.cookies_).forEach(function(domain) {
      if (!filter || domain.indexOf(filter) != -1) {
        result.push(domain);
      }
    });
    return result;
  };
  this.getCookies = function(domain) {
    return this.cookies_[domain];
  };
}
var cache = new CookieCache();

function removeAllForFilter() {
  var filter = select("#filter").value;
  var timer = new Timer();
  cache.getDomains(filter).forEach(function(domain) {
    removeCookiesForDomain(domain);
  });
}

function removeAll() {
  var all_cookies = [];
  cache.getDomains().forEach(function(domain) {
    cache.getCookies(domain).forEach(function(cookie) {
      all_cookies.push(cookie);
    });
  });
  cache.reset();
  var count = all_cookies.length;
  var timer = new Timer();
  for (var i = 0; i < count; i++) {
    removeCookie(all_cookies[i]);
  }
  timer.reset();
  chrome.cookies.getAll({}, function(cookies) {
    for (var i in cookies) {
      cache.add(cookies[i]);
      removeCookie(cookies[i]);
    }
  });
}

function removeCookie(cookie) {
  var url = "http" + (cookie.secure ? "s" : "") + "://" + cookie.domain + cookie.path;
  chrome.cookies.remove({
    "url": url,
    "name": cookie.name
  });
}

function removeCookiesForDomain(domain, type) {
  if (type == 0){
    whiteCookies[domain].forEach(function(cookie) {
      removeCookie(cookie);
    });
  }
  else if (type == 1){
    allCookies[domain].forEach(function(cookie) {
      removeCookie(cookie);
    });
  }
  getCookiesBySite(getCommonSites());
  $("#filter").val("");
  $('#filter').trigger('input');
}

function resetTable() {
  var table = select("#cookies");
  while (table.rows.length > 1) {
    table.deleteRow(table.rows.length - 1);
  }
}
var reload_scheduled = false;

function scheduleReloadCookieTable() {
  if (!reload_scheduled) {
    reload_scheduled = true;
    setTimeout(reloadCookieTable, 250);
  }
}

function reloadCookieTable() {
  reload_scheduled = false;
  var filter = select("#filter").value;
  var domains = cache.getDomains(filter);
  select("#filter_count").innerText = domains.length;
  select("#total_count").innerText = cache.getDomains().length;
  select("#delete_all_button").innerHTML = "";
  if (domains.length) {
    var button = document.createElement("button");
    button.onclick = removeAllForFilter;
    button.innerText = "delete all " + domains.length;
    select("#delete_all_button").appendChild(button);
  }
  resetTable();
  var table = select("#cookies");
  domains.forEach(function(domain) {
    var cookies = cache.getCookies(domain);
    var row = table.insertRow(-1);
    row.insertCell(-1).innerText = domain;
    var cell = row.insertCell(-1);
    cell.innerText = cookies.length;
    cell.setAttribute("class", "cookie_count");
    var button = document.createElement("button");
    button.innerText = "delete";
    button.onclick = (function(dom) {
      return function() {
        removeCookiesForDomain(dom);
      };
    }(domain));
    var cell = row.insertCell(-1);
    cell.appendChild(button);
    cell.setAttribute("class", "button");
  });
}

function focusFilter() {
  select("#filter").focus();
}

function resetFilter() {
  var filter = select("#filter");
  filter.focus();
  if (filter.value.length > 0) {
    filter.value = "";
    reloadCookieTable();
  }
}
var ESCAPE_KEY = 27;
window.onkeydown = function(event) {
  if (event.keyCode == ESCAPE_KEY) {
    resetFilter();
  }
}

function listener(info) {
  cache.remove(info.cookie);
  if (!info.removed) {
    cache.add(info.cookie);
  }
  scheduleReloadCookieTable();
}

function startListening() {
  chrome.cookies.onChanged.addListener(listener);
}

function stopListening() {
  chrome.cookies.onChanged.removeListener(listener);
}

function getCommonSites() {
  var sites = []
  for (var i in window.localStorage) {
    sites.push(i);
  }
  return sites;
}

function setNewSite(site) {
  window.localStorage.setItem(site, 1);
}


function getCookiesBySite(sites) {
  var cks = {};
  var _allCks = {};
  var _cookies = [];
  var _whiteListCookies = [];
  chrome.cookies.getAll({}, function(cookies) {
    cookies_ = cookies.filter(function(cookie) {
      for (var i in sites) {
        if (cookie.domain.indexOf(sites[i]) != -1) return true;
      }
      return false;
    });
    for (var i in cookies_) {
      var hostArr = cookies_[i].domain.split('.');
      var host = hostArr[hostArr.length - 2] + "." + hostArr[hostArr.length - 1];
      if (typeof(cks[host]) === "undefined") cks[host] = [cookies_[i]];
      else cks[host].push(cookies_[i]);
    }
    allCookies_ = cookies.filter(function(cookie) {
      for (var i in sites) {
        if (cookie.domain.indexOf(sites[i]) != -1) return false;
      }
      return true;
    });
    for (var i in allCookies_) {
      var hostArr = allCookies_[i].domain.split('.');
      var host = hostArr[hostArr.length - 2] + "." + hostArr[hostArr.length - 1];
      if (typeof(_allCks[host]) === "undefined") _allCks[host] = [allCookies_[i]];
      else _allCks[host].push(allCookies_[i]);
    }
    allCookies = _allCks;
    whiteCookies = cks;
    allDomains = [];
    for (var k in _allCks) {
      allDomains.push(k);
    }
    showInitialData();
    $("#filter").typeahead('destroy');
    $("#filter").typeahead({
      hint: true,
      highlight: true,
      minLength: 1,
      name: 'allDomains',
      displayKey: 'value',
      source: substringMatcher(allDomains)
    });
    /*
    $("#bookmark").typeahead('destroy');
    $("#bookmark").typeahead({
      hint: true,
      highlight: true,
      minLength: 1,
      name: 'allDomains',
      displayKey: 'value',
      source: substringMatcher(allDomains)
    });
*/
  });
  return {
    'all': _allCks,
    'white': cks
  };
}

function showResultSites(domain){
  if (typeof(allCookies[domain]) != "undefined" && domain != "") {
    var i = domain;
    var html = "<ul class='menu list-group'>";
    html += "<li class='list-group-item'><span class='parent-domain'><img src='http://g.etfv.co/http://www." + i + "' width='20' height='20'></img>&nbsp;&nbsp;&nbsp;" + i + "</span>&nbsp;&nbsp;<i id='delete-" + i + "' class='glyphicon glyphicon-remove'></i><span class='badge'>" + allCookies[i].length + " cookies</span></li>";
    html += "<table style='display: none' class='table table-hover table-striped table-bordered'>";
    html += "<thead>";
    html += "<th style='width: 15%;'>Domain</th><th style='width: 20%;'>Name</th><th style='width: 50%;'>Value</th><th style='width: 15%;'>Expiration Date</th>";
    html += "</thead>";
    html += "<tbody>";
    for (var j in allCookies[i]) {
      html += "<tr>";
      html += "<td>" + allCookies[i][j].domain + "</td>";
      html += "<td>" + allCookies[i][j].name + "</td>";
      html += "<td>" + allCookies[i][j].value + "</td>";
      if (typeof(allCookies[i][j].expirationDate) != "undefined") {
        var d = new Date(allCookies[i][j].expirationDate * 1000);
        html += "<td>" + d.getMonth() + "-" + d.getDate() + "-" + (d.getYear()+1900) + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds() + "</td>";
      }
      else
        html += "<td></td>";
      html += "</tr>";
    }
    html += "</tbody>";
    html += "</table>";
    html += "</ul>";
    document.querySelector("#searchSites").innerHTML = html;
    $("#delete-" + i.replace('.','\\.')).click(function(ev){
      if(confirm("Are you sure you want to delete all cookies from " + i + "?")) removeCookiesForDomain(i, 1);
    });
    $('#searchSites span').click(function() {
      $(this).parent().next('table').toggle();
    });
  }
  else{
    document.querySelector("#searchSites").innerHTML = "";
  }
}

function showInitialData() {
  var html = "<ul class='menu list-group'>";
  for (var i in whiteCookies) {
    html += "<li class='list-group-item'><span class='parent-domain'><img src='http://g.etfv.co/http://www." + i + "' width='20' height='20'></img>&nbsp;&nbsp;&nbsp;" + i + "</span>&nbsp;&nbsp;<i id='delete-" + i + "' class='glyphicon glyphicon-remove'></i><span class='badge'>" + whiteCookies[i].length + " cookies</span></li>";
    html += "<table style='display: none' class='table table-hover table-striped table-bordered'>";
    html += "<thead>";
    html += "<th style='width: 15%;'>Domain</th><th style='width: 20%;'>Name</th><th style='width: 50%;'>Value</th><th style='width: 15%;'>Expiration Date</th>";
    html += "</thead>";
    html += "<tbody>";
    for (var j in whiteCookies[i]) {
      html += "<tr>";
      html += "<td>" + whiteCookies[i][j].domain + "</td>";
      html += "<td>" + whiteCookies[i][j].name + "</td>";
      html += "<td>" + whiteCookies[i][j].value + "</td>";
      if (typeof(whiteCookies[i][j].expirationDate) != "undefined") {
        var d = new Date(whiteCookies[i][j].expirationDate * 1000);
        html += "<td>" + d.getMonth() + "-" + d.getDate() + "-" + (d.getYear()+1900) + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds() + "</td>";
      }
      else
        html += "<td></td>";
      html += "</tr>";
    }
    html += "</tbody>";
    html += "</table>";
  }
  html += "</ul>";
  document.querySelector("#commonSites").innerHTML = html;
  for (var i in whiteCookies) {
    $("#delete-" + i.replace('.','\\.')).click(function(ev){
      if(confirm("Are you sure you want to delete all cookies from " + i + "?")) removeCookiesForDomain(i, 0);
    });
  }
  $('#commonSites span').click(function() {
    $(this).parent().next('table').toggle();
  });
  $("#filter").on('input', function(){
    showResultSites($(this)[0].value);
  }).on('change', function(){
    showResultSites($(this)[0].value);
  }).trigger('input');
}

var substringMatcher = function(strs) {
  return function findMatches(q, cb) {
    var matches, substringRegex;
    // an array that will be populated with substring matches
    matches = [];
    // regex used to determine if a string contains the substring `q`
    substrRegex = new RegExp(q, 'i');
    // iterate through the pool of strings and for any string that
    // contains the substring `q`, add it to the `matches` array
    $.each(strs, function(i, str) {
      if (substrRegex.test(str)) {
        // the typeahead jQuery plugin expects suggestions to a
        // JavaScript object, refer to typeahead docs for more info
        matches.push({
          value: str
        });
      }
    });
    cb(matches);
  };
};
document.addEventListener('DOMContentLoaded', function() {
  focusFilter();
  console.log(getCookiesBySite(getCommonSites()));
});
